-module(reg_handler).
-behaviour(cowboy_rest).
-export([init/2]).
-export([allowed_methods/2]).
-export([content_types_accepted/2]).
-export([reg_user/2]).

init(Req, State) -> {cowboy_rest, Req, State}.

allowed_methods(Req, State) ->
    {[<<"POST">>], Req, State}.

content_types_accepted(Req, State) ->
    {[
        {{<<"application">>, <<"json">>, [{<<"charset">>, <<"utf-8">>}]}, reg_user}
    ], Req, State}.

reg_user(Req, State) ->
    {ok, Data, Req2} = rest_util:read_body(Req),
    case rest_util:parse_json(Data) of
        {ok, #{<<"login">> := Login, <<"password">> := Password}} ->
            case rest_db:create_user(Login, Password) of
                {ok, _} -> rest_util:ok(#{result => ok}, Req2, State);
                {error, <<"23000">>, _} -> rest_util:bad_request(#{result => error, error => <<"User with such login is already registered">>}, Req2, State);
                {error, _, E} -> rest_util:error500(#{result => error, error => iolist_to_binary(E)}, Req2, State) end;
        _ ->
            rest_util:bad_request(#{result => error, error => <<"Fields required: login, password">>}, Req2, State)
    end.
