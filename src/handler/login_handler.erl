-module(login_handler).
-behaviour(cowboy_rest).

-export([init/2]).
-export([allowed_methods/2]).
-export([content_types_accepted/2]).
-export([do_auth/2]).
% -compile(export_all).

init(Req, State) -> {cowboy_rest, Req, State}.

allowed_methods(Req, State) ->
    {[<<"POST">>], Req, State}.

content_types_accepted(Req, State) ->
    {[
        {{<<"application">>, <<"json">>, [{<<"charset">>, <<"utf-8">>}]}, do_auth}
    ], Req, State}.

do_auth(Req, State) ->
    {ok, Data, Req2} = rest_util:read_body(Req),
    case rest_util:parse_json(Data) of
        {ok, #{<<"login">> := Login, <<"password">> := Password}} ->
            check_auth(Login, Password, Req, State);
        _ ->
            rest_util:bad_request(#{auth => false, error => <<"Fields required: login, password">>}, Req2, State)
    end.

check_auth(Login, Password, Req, State) ->
    case rest_auth:check(Login, Password) of
        {ok, UserId} -> rest_util:ok(#{auth => true, token => rest_auth:sign(UserId)}, Req, State);
        {error, _} -> rest_util:unauthorized(#{auth => false, error => <<"Wrong credentials">>}, Req, State)
    end.
