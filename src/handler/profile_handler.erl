-module(profile_handler).
-behaviour(cowboy_rest).

-export([init/2]).
-export([content_types_accepted/2]).
-export([allowed_methods/2]).
-export([is_authorized/2]).
-export([change_pass/2]).

init(Req, State) -> {cowboy_rest, Req, State}.

allowed_methods(Req, State) ->
    {[<<"PUT">>], Req, State}.

is_authorized(Req, State) ->
    Unautorized = {{false, "Bearer realm=\"rest_app\""}, Req, State},
    case cowboy_req:parse_header(<<"authorization">>, Req) of
        {bearer, Token} ->
            case rest_auth:check(Token) of
                {ok, UserId} -> {true, Req, UserId}; 
                _ -> Unautorized
            end;
        _ ->
            Unautorized
    end.

content_types_accepted(Req, State) ->
    {[
        {{<<"application">>, <<"json">>, [{<<"charset">>, <<"utf-8">>}]}, change_pass}
    ], Req, State}.

change_pass(Req, UserId) ->
    {ok, Data, Req2} = rest_util:read_body(Req),
    case rest_util:parse_json(Data) of
        {ok, #{<<"old_password">> := Old, <<"new_password">> := New}} ->
            {ok, #{<<"password">> := Password}} = rest_db:get_user(UserId),
            case rest_util:hash(Old) =:= Password of
                true  -> rest_db:change_pass(UserId, New),
                         rest_util:ok(#{result => ok}, Req2, UserId);
                false -> rest_util:bad_request(#{result => error, error => <<"Wrong old password">>}, Req2, UserId)
            end;
        _ ->
            rest_util:bad_request(#{result => error, error => <<"Fields required: old_password, new_password">>}, Req2, UserId)
    end.
