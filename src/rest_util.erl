-module(rest_util).
-export([read_body/1, parse_json/1]).
-export([ok/3, bad_request/3, unauthorized/3, error500/3]).
-export([hash/1]).
-export([reply/3, reply/4]).

read_body(Req0) -> read_body(Req0, <<>>).

read_body(Req0, Acc) ->
    case cowboy_req:read_body(Req0) of
        {ok, Data, Req} -> {ok, << Acc/binary, Data/binary >>, Req};
        {more, Data, Req} -> read_body(Req, << Acc/binary, Data/binary >>)
    end.

parse_json(Data) ->
    case jsx:is_json(Data) of
        true -> {ok, jsx:decode(Data, [{return_maps, true}])};
        false -> {error, invalid_json} end.

hash(Data) ->
    <<X:256/big-unsigned-integer>> = crypto:hash(sha256,Data),
    iolist_to_binary(io_lib:format("~64.16.0b", [X])).

ok(Body, Req, State)           -> reply(200, Body, Req, State).
bad_request(Body, Req, State)  -> reply(400, Body, Req, State).
unauthorized(Body, Req, State) -> reply(401, Body, Req, State).
error500(Body, Req, State)     -> reply(500, Body, Req, State).

reply(Code, Req, State) ->
    Req2 = cowboy_req:reply(Code, #{}, Req),
    {stop, Req2, State}.

reply(Code, Body, Req, State) ->
    Req2 = cowboy_req:reply(Code, #{}, jsx:encode(Body), Req),
    {stop, Req2, State}.
