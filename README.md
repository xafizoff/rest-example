# An example REST service

This is an example RESTful service with the following features (methods):
* New user registration
* User authentication (login)
* User data management (changing user password)
* All users list

# Building and running

## Prerequisites

* MySQL server >= 5.0
* Elixir >= 1.10

To start the service, run the following commands in a UNIX shell:

``` shell
$ git clone https://gitlab.com/xafizoff/rest-example.git
$ cd rest-example
$ mix deps.get
$ DB_USER=my_user DB_PASS=my_pass iex -S mix
```

There are several environment variables that can be set:
* `APP_PORT` - server port, default: `8000`
* `DB_USER` - MySQL username, default: `rest_user`
* `DB_PASSWORD` - MySQL user password, default: `rest_pass`
* `DB_HOST` - MySQL server hostname, default: `127.0.0.1`
* `DB_PORT` - MySQL server port, default: `3306`
* `DB_DATABASE` - MySQL database name, default: `rest_db`

# Methods overview

## New user registration

``` shell
$ curl -XPOST http://localhost:8000/reg -H 'Content-Type: application/json;charset=utf-8' -d '{"login": "handsome", "password": "secret"}'
{"result": "ok"}
$ curl -XPOST http://localhost:8000/reg -H 'Content-Type: application/json;charset=utf-8' -d '{"login": "handsome", "password": "secret"}'
{"error":"User with such login is already registered","result":"error"}
```

## User authentication (login)

``` shell
$ curl -XPOST http://localhost:8000/login -H 'Content-Type: application/json;charset=utf-8' -d '{"login": "handsome", "password": "secret"}'
{"auth":true,"token":"eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTF9.9uMXWKg8qsKzXxf2Qr8a1zSwRySp7UR0U4cR92i2lRoi6jKELLT1jbJ55X4un_JND3sy_HykkbbaRUajOFSBmQ"}
$ curl -XPOST http://localhost:8000/login -H 'Content-Type: application/json;charset=utf-8' -d '{"login": "handsome", "password": "wrong_pass"}'
{"auth":false,"error":"Wrong credentials"}
```

## Changing user password

This method must be authorized. Use the token obtained with login method in the `Bearer` authorization header:

``` shell
$ curl -XPUT http://localhost:8000/profile -H 'Content-Type: application/json;charset=utf-8' -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTF9.9uMXWKg8qsKzXxf2Qr8a1zSwRySp7UR0U4cR92i2lRoi6jKELLT1jbJ55X4un_JND3sy_HykkbbaRUajOFSBmQ' -d '{"old_password": "secret", "new_password": "strong_pass"}'
{"result":"ok"}
$ curl -XPUT http://localhost:8000/profile -H 'Content-Type: application/json;charset=utf-8' -H 'Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTF9.9uMXWKg8qsKzXxf2Qr8a1zSwRySp7UR0U4cR92i2lRoi6jKELLT1jbJ55X4un_JND3sy_HykkbbaRUajOFSBmQ' -d '{"old_password": "bad_pass", "new_password": "whatever"}'
{"error":"Wrong old password","result":"error"}
$ curl -XPUT http://localhost:8000/profile -H 'Content-Type: application/json;charset=utf-8' -H 'Authorization: Bearer invalidToken' -d '{"old_password": "bad_pass", "new_password": "whatever"}' -v
...
< HTTP/1.1 401 Unauthorized
< content-length: 0
< date: Thu, 27 Aug 2020 14:47:12 GMT
< server: Cowboy
< www-authenticate: Bearer realm="rest_app"
< 
* Connection #0 to host localhost left intact
```

## Getting all users list

``` shell
$ curl http://localhost:8000/user
[{"id":1,"login":"handsome","password":"***"}]
```

# Authorization

In this service uses token-based authorization. We use JWT as a token.

## Not implemented
* Token expiration
* Refresh tokens
