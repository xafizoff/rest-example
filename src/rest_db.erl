-module(rest_db).

-include_lib("emysql/include/emysql.hrl").

-export([migrate/0]).
-export([find_user/1]).
-export([get_user/1]).
-export([create_user/2]).
-export([change_pass/2]).
-export([all_users/0]).

migrate() ->
    emysql:execute(mysql_pool, "create table if not exists user ("
                               "id integer primary key auto_increment, login varchar(128) unique not null, password varchar(128) not null"
                               ");").

execute(Query, Params) ->
    emysql:prepare(my_stmt, Query),
    Res = emysql:execute(mysql_pool, my_stmt, Params),
    case Res of
        #result_packet{} -> emysql:as_maps(Res);
        _ -> Res end.

create_user(Login, Password) ->
    case execute("insert into user(login, password) values(?,?)", [string:lowercase(Login), rest_util:hash(Password)]) of
        #ok_packet{insert_id = Id} -> {ok, Id};
        #error_packet{status = Code, msg = Error} -> {error, Code, Error};
        Else -> Else
    end.

change_pass(Id, Password) ->
    execute("update user set password = ? where id = ?", [rest_util:hash(Password), Id]).

all_users() ->
    execute("select id, login, '***' as password from user", []).

get_user(Id) ->
    case execute("select id, login, password from user where id = ?", [Id]) of
        [User|_] -> {ok, User};
        _ -> {error, not_found}
    end.

find_user(Login) ->
    case execute("select id, login, password from user where login = ?", [string:lowercase(Login)]) of
        [User|_] -> {ok, User};
        _ -> {error, not_found}
    end.
