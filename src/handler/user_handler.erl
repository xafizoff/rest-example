-module(user_handler).
-behaviour(cowboy_rest).
-export([init/2]).
-export([allowed_methods/2]).
-export([content_types_provided/2]).
-export([get_user_list/2]).

init(Req, State) -> {cowboy_rest, Req, State}.

allowed_methods(Req, State) ->
    {[<<"GET">>], Req, State}.

content_types_provided(Req, State) ->
    {[
        {{<<"application">>, <<"json">>, '*'}, get_user_list}
    ], Req, State}.

get_user_list(Req, State) ->
    Users = rest_db:all_users(),
    {jsx:encode(Users), Req, State}.
