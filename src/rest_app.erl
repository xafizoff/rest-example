-module(rest_app).
-behaviour(application).
-export([start/2, stop/1]).

start(_Type, _Args) -> start_cowboy(), start_db_pool(), rest_db:migrate(), rest_sup:start_link().
stop(_) -> ok = cowboy:stop_listener(http).

start_cowboy() ->
    cowboy:start_clear(http, [{port, port()}],
          #{ env => #{dispatch => routes()} }).

start_db_pool() ->
    Conf = application:get_env(rest_app, mysql_pool, []),
    emysql:add_pool(mysql_pool, Conf).

port() -> application:get_env(rest_app, port, 8000).

routes() -> cowboy_router:compile([{'_', [ { "/login", login_handler, [] },
                                           { "/reg", reg_handler, []},
                                           { "/profile", profile_handler, []},
                                           { "/user", user_handler, []}
                                        ]}]).
