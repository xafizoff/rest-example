-module(rest_auth).
-export([sign/1, verify/1]).
-export([check/1, check/2]).

-define(SECRET, application:get_env(rest_app, secret, <<"secret_key123">>)).

sign(Id) ->
    jwerl:sign([{id, Id}], hs512, ?SECRET).

verify(Token) ->
    try
        jwerl:verify(Token, hs512, ?SECRET)
    catch
        E:R:S -> {E,R,S}
    end.

check(Token) ->
    case verify(Token) of
        {ok, #{id := Id}} when is_integer(Id) ->
            case rest_db:get_user(Id) of
                {ok,_} -> {ok, Id};
                E -> E
            end;
        _ -> false end.

check(Login, Password) ->
    case rest_db:find_user(Login) of
        {error, not_found} -> {error, not_found};
        {ok, #{<<"password">> := Pwd, <<"id">> := Id}} ->
            case rest_util:hash(Password) =:= Pwd of
                true -> {ok, Id};
                false -> {error, wrong_password}
            end
    end.
