defmodule RestExample.MixProject do
  use Mix.Project

  def project do
    [
      app: :rest_app,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      mod: {:rest_app, []},
      extra_applications: [:logger, :crypto]
    ]
  end

  defp deps do
    [
      {:cowboy, "~> 2.8"},
      {:jwerl, "~> 1.1.0"},
      {:emysql, github: "inaka/Emysql", tag: "0.5.0"},
    ]
  end
end
