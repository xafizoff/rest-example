use Mix.Config

config :rest_app,
    port: (System.get_env("APP_PORT") || "8000") |> :erlang.binary_to_integer,
    secret: "3934660b466370a0768f3b86cb5f93184b662e4bc0f654c9fe2b1f80c7e86278"

config :rest_app, :mysql_pool,
    host: (System.get_env("DB_HOST") || "127.0.0.1") |> String.to_charlist,
    port: (System.get_env("DB_PORT") || "3306") |> :erlang.binary_to_integer,
    user: (System.get_env("DB_USER") || "rest_user") |> String.to_charlist,
    password: (System.get_env("DB_PASSWORD") || "rest_pass") |> String.to_charlist,
    database: (System.get_env("DB_DATABASE") || "rest_db") |> String.to_charlist,
    size: 1
